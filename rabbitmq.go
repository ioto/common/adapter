package adapter

import (
	"encoding/json"
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/streadway/amqp"
)

// Rabbits ..
type Rabbits map[string]*Rabbit

// Get Rabbit
func (adapters Rabbits) Get(name string) (result *Rabbit) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Không tìm thấy config Rabbit " + name)
	}
	return
}

// Rabbit ..
type Rabbit struct {
	Name       string          `mapstructure:"name"`
	Host       string          `mapstructure:"host"`
	Port       string          `mapstructure:"port"`
	Vhost      string          `mapstructure:"vhost"`
	User       string          `mapstructure:"user"`
	Pass       string          `mapstructure:"pass"`
	Consumers  RabbitConsumers `mapstructure:"consumers"`
	Channel    *amqp.Channel
	Connection *amqp.Connection
}

// RabbitConsumers ..
type RabbitConsumers map[string]RabbitConsumer

// RabbitConsumer ..
type RabbitConsumer struct {
	Queue      string `mapstructure:"queue"`
	Exchange   string `mapstructure:"exchange"`
	RoutingKey string `mapstructure:"routing_key"`
	Prefetch   int    `mapstructure:"prefetch"`
}

var onceRabbit map[string]*sync.Once
var onceRabbitMutex = sync.RWMutex{}
var onceRabbitChannel map[string]*sync.Once

func init() {
	onceRabbit = make(map[string]*sync.Once)
	onceRabbitChannel = make(map[string]*sync.Once)
}

// Init func
func (config *Rabbit) Init() {
	config.InitConnection()
	config.InitChannel()
}

// InitConnection func
func (config *Rabbit) InitConnection() {
	if onceRabbit[config.Name] == nil {
		onceRabbit[config.Name] = &sync.Once{}
	}
	onceRabbit[config.Name].Do(func() {
		onceRabbitMutex.Lock()
		log.Printf("[%s][%s] RabbitMQ [connecting]\n", config.Name, config.Host)
		connString := fmt.Sprintf("amqp://%s:%s@%s:%s/%s", config.User, config.Pass, config.Host, config.Port, config.Vhost)
		var err error
		config.Connection, err = amqp.Dial(connString)
		if err != nil {
			log.Printf("[%s][%s] RabbitMQ [error]: %s", config.Name, config.Host, err)
			time.Sleep(1 * time.Second)
			onceRabbit[config.Name] = &sync.Once{}
			onceRabbitMutex.Unlock()
			config.Init()
			return
		}
		log.Printf("[%s][%s] RabbitMQ [connected]\n", config.Name, config.Host)
		onceRabbitMutex.Unlock()
		// reconnect
		closeError := make(chan *amqp.Error)
		config.Connection.NotifyClose(closeError)
		go func() {
			for err := range closeError {
				log.Printf("[%s][%s] RabbitMQ [error]: %s", config.Name, config.Host, err)
				time.Sleep(1 * time.Second)
				onceRabbit[config.Name] = &sync.Once{}
				config.Init()
				return
			}
		}()
	})
}

// InitChannel func
func (config *Rabbit) InitChannel() {
	if onceRabbitChannel[config.Name] == nil {
		onceRabbitChannel[config.Name] = &sync.Once{}
	}
	onceRabbitChannel[config.Name].Do(func() {
		onceRabbitMutex.Lock()
		log.Printf("[%s][%s] RabbitMQ Channel [connecting]\n", config.Name, config.Host)
		var err error
		config.Channel, err = config.Connection.Channel()
		if err != nil {
			log.Printf("[%s][%s] RabbitMQ Channel [error]: %s", config.Name, config.Host, err)
			time.Sleep(1 * time.Second)
			onceRabbitChannel[config.Name] = &sync.Once{}
			onceRabbitMutex.Unlock()
			config.InitChannel()
			return
		}
		log.Printf("[%s][%s] RabbitMQ Channel [connected]\n", config.Name, config.Host)
		onceRabbitMutex.Unlock()

		// reconnect
		closeErrorChannel := make(chan *amqp.Error)
		config.Channel.NotifyClose(closeErrorChannel)
		go func() {
			for err := range closeErrorChannel {
				log.Printf("[%s][%s] RabbitMQ Channel [error]: %s", config.Name, config.Host, err)
				time.Sleep(1 * time.Second)
				onceRabbitChannel[config.Name] = &sync.Once{}
				config.InitChannel()
				return
			}
		}()
	})
}

// Get RabbitConsumer
func (consumers RabbitConsumers) Get(name string) (consumer RabbitConsumer) {
	if data, ok := consumers[name]; ok {
		consumer = data
	}
	return
}

// Prepare func
func (config *Rabbit) Prepare(consumer RabbitConsumer) (err error) {

	if config.Connection == nil {
		err = fmt.Errorf("[%s][%s] Chưa init Rabbit", config.Name, config.Host)
		return
	}

	if config.Channel == nil {
		err = fmt.Errorf("[%s][%s] Chưa init channel Rabbit", config.Name, config.Host)
		return
	}

	err = config.Channel.Qos(
		consumer.Prefetch, // prefetch count
		0,                 // prefetch size
		false,             // global
	)
	if err != nil {
		return
	}

	q, err := config.Channel.QueueDeclare(
		consumer.Queue, // name
		true,           // durable
		false,          // delete when unused
		false,          // exclusive
		false,          // no-wait
		nil,            // arguments
	)
	if err != nil {
		return
	}

	err = config.Channel.ExchangeDeclare(
		consumer.Exchange, // name
		"topic",           // type
		true,              // durable
		false,             // auto-deleted
		false,             // internal
		false,             // no-wait
		nil,               // arguments
	)
	if err != nil {
		return
	}

	err = config.Channel.QueueBind(
		q.Name,              // queue name
		consumer.RoutingKey, // routing key
		consumer.Exchange,   // exchange
		false,
		nil,
	)
	if err != nil {
		return
	}

	return
}

// Consume ..
func (config Rabbit) Consume(consumer RabbitConsumer) (msgs <-chan amqp.Delivery, err error) {
	return config.Channel.Consume(
		consumer.Queue, // queue
		"",             // consumer
		false,          // auto-ack
		false,          // exclusive
		false,          // no-local
		false,          // no-wait
		nil,            // args
	)
}

// Publish ..
func (config Rabbit) Publish(consumer RabbitConsumer, data interface{}) (err error) {
	body, err := json.Marshal(data)
	if err != nil {
		return
	}

	return config.Channel.Publish(
		consumer.Exchange,   // exchange
		consumer.RoutingKey, // routing key
		false,               // mandatory
		false,               // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        []byte(body),
		})
}
