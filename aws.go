package adapter

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"log"
	"sync"
	"time"
)

// SyncAwsOnes ..
type SyncAwsOnes map[string]*AwsCloudStorage

// Get AwsCloudStorage
func (adapters SyncAwsOnes) Get(name string) (result *AwsCloudStorage) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Không tìm thấy config AwsCloudStorage " + name)
	}
	return
}

var (
	syncAwsOnes map[string]*sync.Once
	onceMutex   = sync.RWMutex{}
)

func init() {
	syncAwsOnes = make(map[string]*sync.Once)
}

type AwsCloudStorage struct {
	Name       string `mapstructure:"name"`
	BucketName string `mapstructure:"bucket_name"`
	Region     string `mapstructure:"region"`
	AccessKey  string `mapstructure:"access_key"`
	SecretKey  string `mapstructure:"secret_key"`
	S3Client   *s3.S3
}

func (as *AwsCloudStorage) Init() {
	onceMutex.Lock()

	if syncAwsOnes[as.Name] == nil {
		syncAwsOnes[as.Name] = &sync.Once{}
	}

	syncAwsOnes[as.Name].Do(func() {
		creds := credentials.NewStaticCredentials(as.AccessKey, as.SecretKey, "")
		awsCfg := aws.NewConfig().WithRegion(as.Region).WithCredentials(creds)
		as.S3Client = s3.New(session.New(), awsCfg)
	})
	onceMutex.Unlock()

	if as.S3Client == nil {
		log.Printf("AwsCloud Connect [error]: Retring... \n")
		time.Sleep(1 * time.Second)
		syncAwsOnes[as.Name] = &sync.Once{}
		as.Init()
		return
	}
}
