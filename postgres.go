package adapter

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/go-xorm/xorm"
	_ "github.com/lib/pq" // postgre lib for xorm
)

// Postgres ..
type Postgres map[string]*Postgre

// Get Postgre
func (adapters Postgres) Get(name string) (result *Postgre) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Không tìm thấy config Postgre " + name)
	}
	return
}

// Postgre ..
type Postgre struct {
	Name    string `mapstructure:"name"`
	Server  string `mapstructure:"server"`
	Port    int    `mapstructure:"port"`
	DBName  string `mapstructure:"dbname"`
	User    string `mapstructure:"user"`
	Pass    string `mapstructure:"password"`
	Session *xorm.Engine
}

var (
	oncePG      map[string]*sync.Once
	oncePGMutex = sync.RWMutex{}
)

func init() {
	oncePG = make(map[string]*sync.Once)
}

// Init ..
func (config *Postgre) Init() {
	if oncePG[config.Name] == nil {
		oncePG[config.Name] = &sync.Once{}
	}
	onceSQL[config.Name].Do(func() {
		oncePGMutex.Lock()
		log.Printf("[%s][%s] Postgre [connecting]", config.Name, config.Server)
		connString := fmt.Sprintf("postgresql://%v:%v@%v/%v", config.User, config.Pass, config.Server, config.DBName)
		xormEngine, err := xorm.NewEngine("postgres", connString)
		if err != nil {
			log.Printf("[%s][%s] Postgre [error]: %s", config.Name, config.Server, err)
			time.Sleep(1 * time.Second)
			oncePG[config.Name] = &sync.Once{}
			oncePGMutex.Unlock()
			config.Init()
		} else {
			config.Session = xormEngine
			config.Session.SetMaxOpenConns(100)
			config.Session.SetMaxIdleConns(5)
			config.Session.ShowSQL(true)
			config.Session.ShowExecTime()
			//connect thử
			errPing := config.Session.Ping()
			if errPing != nil {
				log.Printf("[%s][%s] Postgre [ping error]: %s", config.Name, config.Server, errPing)
				time.Sleep(1 * time.Second)
				oncePG[config.Name] = &sync.Once{}
				oncePGMutex.Unlock()
				config.Init()
				return
			}
			log.Printf("[%s][%s] Postgre [connected]", config.Name, config.Server)
			oncePGMutex.Unlock()
		}
	})
}

// GetPostgreSession func
func (config Postgre) GetPostgreSession() (session *xorm.Session) {
	if config.Session != nil {
		session = config.Session.NewSession()
	} else {
		panic(fmt.Errorf("[%s][%s] Chưa init Postgre", config.Name, config.Server))
	}
	return
}
