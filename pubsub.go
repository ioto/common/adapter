package adapter

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"google.golang.org/api/option"
	"log"
	"sync"
	"time"

	"cloud.google.com/go/pubsub"
)

// GooglePubSubs ..
type GooglePubSubs map[string]*GooglePubSub

// Get GooglePubSub
func (adapters GooglePubSubs) Get(name string) (result *GooglePubSub) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Không tìm thấy config GooglePubSubs " + name)
	}
	return
}

// GooglePubSub ..
type GooglePubSub struct {
	Name        string              `mapstructure:"name"`
	ProjectID   string              `mapstructure:"project_id"`
	TopicID     string              `mapstructure:"topic_id"`
	Certificate string              `mapstructure:"certificate"`
	Consumers   GoogleSubscriptions `mapstructure:"consumers"`
	Client      *pubsub.Client
	Topic       *pubsub.Topic
}

// GoogleSubscriptions ..
type GoogleSubscriptions map[string]Subscription

// Subscription ..
type Subscription struct {
	SubscriptionName string `mapstructure:"subscription_name"`
	Subscription     *pubsub.Subscription
}

var oncePubSub map[string]*sync.Once
var oncePubSubMutex sync.RWMutex
var oncePubSubTopic map[string]*sync.Once

func init() {
	oncePubSub = make(map[string]*sync.Once)
	oncePubSubTopic = make(map[string]*sync.Once)
	oncePubSubMutex = sync.RWMutex{}
}

// InitConnection func
func (config *GooglePubSub) Init() {
	config.InitConnection()
	config.InitTopic()
}

// InitConnection func
func (config *GooglePubSub) InitConnection() {
	if oncePubSub[config.Name] == nil {
		oncePubSub[config.Name] = &sync.Once{}
	}
	oncePubSub[config.Name].Do(func() {
		oncePubSubMutex.Lock()
		log.Printf("[%s][%s] GooglePubSub [connecting]\n", config.Name, config.ProjectID)
		var err error
		var certificateByte []byte
		certificateByte, err = base64.StdEncoding.DecodeString(config.Certificate)
		if err != nil {
		} else {
			opts := option.WithCredentialsJSON(certificateByte)
			config.Client, err = pubsub.NewClient(context.TODO(), config.ProjectID, opts)
			if err != nil {
				log.Printf("[%s][%s] GooglePubSub [error]: %s", config.Name, config.ProjectID, err)
				time.Sleep(1 * time.Second)
				oncePubSub[config.Name] = &sync.Once{}
				oncePubSubMutex.Unlock()
				config.InitConnection()
				return
			}
			log.Printf("[%s][%s] GooglePubSub [connected]\n", config.Name, config.ProjectID)
			oncePubSubMutex.Unlock()
		}
	})
}

// InitTopic func
func (config *GooglePubSub) InitTopic() {
	if oncePubSubTopic[config.TopicID] == nil {
		oncePubSubTopic[config.TopicID] = &sync.Once{}
	}
	oncePubSubTopic[config.TopicID].Do(func() {
		oncePubSubMutex.Lock()
		log.Printf("[%s][%s] GooglePubSub Topic [connecting]\n", config.Name, config.TopicID)
		config.Topic = config.Client.Topic(config.TopicID)
		ctx := context.TODO()
		isExist, err := config.Topic.Exists(ctx)
		if err != nil {
			log.Printf("[%s][%s] GooglePubSub Topic [error]: %s", config.Name, config.TopicID, err)
			time.Sleep(1 * time.Second)
			oncePubSubTopic[config.Name] = &sync.Once{}
			oncePubSubMutex.Unlock()
			config.Init()
			return
		}

		if !isExist {
			log.Printf("[%s][%s][%s] GooglePubSub Create Topic ...", config.Name, config.ProjectID, config.TopicID)
			config.Topic, err = config.Client.CreateTopic(ctx, config.TopicID)
			if err != nil {
				log.Printf("[%s][%s][%s] GooglePubSub Create Topic [error]: %s", config.Name, config.ProjectID, config.TopicID, err)
				time.Sleep(1 * time.Second)
				oncePubSubTopic[config.Name] = &sync.Once{}
				oncePubSubMutex.Unlock()
				config.Init()
				return
			}
		}
		log.Printf("[%s][%s][%s] GooglePubSub Topic [created]\n", config.Name, config.ProjectID, config.TopicID)
		log.Printf("[%s][%s][%s] GooglePubSub Topic [connected]\n", config.Name, config.ProjectID, config.TopicID)
		oncePubSubMutex.Unlock()
	})
}

// InitSubscription func
func (config *GooglePubSub) Prepare(Name string) (subs *pubsub.Subscription, err error) {

	if config.Client == nil {
		err = fmt.Errorf("[%s][%s] Chưa init GooglePubSub", config.Name, config.ProjectID)
		return
	}

	if config.Topic == nil {
		err = fmt.Errorf("[%s][%s] Chưa init Topic GooglePubSub", config.Name, config.Topic)
		return
	}

	ctx := context.Background()

	Subs := config.Consumers.Get(Name)
	if Subs.SubscriptionName == "" {
		err = fmt.Errorf("[%s][%s] Không tìm thấy config Subscription %s", config.Name, config.ProjectID, Name)
		panic(err)
	}

	var isExist bool
	isExist, err = config.Client.Subscription(Subs.SubscriptionName).Exists(ctx)
	if err != nil {
		return
	}

	if !isExist {
		Subs.Subscription, err = config.Client.CreateSubscription(ctx, Subs.SubscriptionName, pubsub.SubscriptionConfig{
			Topic: config.Topic,
		})

		if err != nil {
			return
		}
	}
	subs = config.Client.Subscription(Subs.SubscriptionName)
	return
}

// Get Subscription
func (subscriptions GoogleSubscriptions) Get(name string) (consumer Subscription) {
	if data, ok := subscriptions[name]; ok {
		consumer = data
	}
	return
}

// Set Subscription
func (subscriptions GoogleSubscriptions) checkAndSet(name string, consumer Subscription) () {
	if _, ok := subscriptions[name]; !ok {
		subscriptions[name] = consumer
	}
	return
}

// Publish ..
func (config GooglePubSub) Publish(ctx context.Context, data interface{}) (err error) {
	body, err := json.Marshal(data)
	if err != nil {
		return
	}

	_, err = config.Topic.Publish(ctx, &pubsub.Message{Data: body, PublishTime: time.Now()}).Get(ctx)
	return
}