package adapter

import (
	"fmt"
	"log"
	"runtime"

	// "runtime"
	"sync"

	graylog "github.com/gemnasium/logrus-graylog-hook/v3"
	"github.com/kr/pretty"
	"github.com/sirupsen/logrus"
)

var extraDataMutex = sync.RWMutex{}

// Graylogs ..
type Graylogs map[string]Graylog

// Get Graylog
func (adapters Graylogs) Get(name string) (result Graylog) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		log.Printf("Không tìm thấy config Graylog %v\n", name)
	}
	return
}

// Graylog struct
type Graylog struct {
	Name    string                 `mapstructure:"name"`
	Address string                 `mapstructure:"address"`
	Extra   map[string]interface{} `mapstructure:"extra"`
}

func (config Graylog) isExist() bool {
	return config.Name != ""
}

// Println func
func (config Graylog) Println(value interface{}, args ...map[string]interface{}) {
	log.Println(value)
}

// Pretty func
func (config Graylog) Pretty(data ...interface{}) {
	pretty.Println(data)
}

// Info func
func (config Graylog) Info(value interface{}, args ...map[string]interface{}) {
	config.write("info", value, args...)
}

// Error func
func (config Graylog) Error(value interface{}, args ...map[string]interface{}) {
	config.write("error", value, args...)
}

// Warn func
func (config Graylog) Warn(value interface{}, args ...map[string]interface{}) {
	config.write("warn", value, args...)
}

// LogRequest func
func (config Graylog) LogRequest(value interface{}, args ...map[string]interface{}) {
	config.write("info", value, args...)
}

func (config Graylog) write(logType string, value interface{}, args ...map[string]interface{}) {

	dataExtra := make(map[string]interface{})
	for k, v := range config.Extra {
		dataExtra[k] = v
	}

	// Nếu panic hoặc error thì log lại toàn bộ stack
	if logType == "panic" || logType == "error" {
		i := 0
		stack := ""
		for {
			i++
			if _, file, line, ok := runtime.Caller(i); ok {
				stack += fmt.Sprintf("%v:%v\n", file, line)
			} else {
				break
			}
		}
		dataExtra["stack"] = stack
	}

	if _, file, line, ok := runtime.Caller(2); ok {
		dataExtra["s-line"] = line
		dataExtra["s-file"] = file
	}

	for _, arg := range args {
		for k, v := range arg {
			dataExtra[k] = v
		}
	}

	log := logrus.New()

	customFormatter := new(logrus.TextFormatter)
	customFormatter.FullTimestamp = true
	log.SetFormatter(customFormatter)

	if config.isExist() {
		hook := graylog.NewGraylogHook(config.Address, dataExtra)
		log.AddHook(hook)
	}

	switch logType {
	case "info":
		log.Infoln(value)
	case "warn":
		log.Warnln(value)
	case "error":
		log.Errorln(value)
	}
}
