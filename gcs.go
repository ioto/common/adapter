package adapter

import (
	"cloud.google.com/go/storage"
	"context"
	"encoding/base64"
	"google.golang.org/api/option"
	"log"
	"sync"
	"time"
)

// GCloudStorages ..
type GCloudStorages map[string]*GoogleCloudStorage

// Get GCloudStorage
func (adapters GCloudStorages) Get(name string) (result *GoogleCloudStorage) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Không tìm thấy config GoogleCloudStorage " + name)
	}
	return
}

var (
	syncGCSOnes  map[string]*sync.Once
	onceGCSMutex = sync.RWMutex{}
)

type GoogleCloudStorage struct {
	Name          string `mapstructure:"name"`
	BucketName    string `mapstructure:"bucket_name"`
	Certificate   string `mapstructure:"certificate"`
	StorageClient *storage.Client
	ClientOption  option.ClientOption
}

func init() {
	syncGCSOnes = make(map[string]*sync.Once)
}

func (gs *GoogleCloudStorage) Init() {
	onceGCSMutex.Lock()
	var connectError error

	if syncGCSOnes[gs.Name] == nil {
		syncGCSOnes[gs.Name] = &sync.Once{}
	}

	syncGCSOnes[gs.Name].Do(func() {
		// Load config
		data, err := base64.StdEncoding.DecodeString(gs.Certificate)
		if err != nil {
			connectError = err
		} else {
			gs.StorageClient, err = storage.NewClient(context.Background(), option.WithCredentialsJSON(data))
			if err != nil {
				connectError = err
			}
		}
	})

	onceGCSMutex.Unlock()

	if connectError != nil {
		log.Printf("GoogleCloudStorage Connect [error]: %v \n", connectError)
		time.Sleep(1 * time.Second)
		syncGCSOnes[gs.Name] = &sync.Once{}
		gs.Init()
		return
	}
}
