package adapter

import (
	"fmt"
	"log"
	"sync"
	"time"

	_ "github.com/denisenkom/go-mssqldb" // sql lib for xorm
	"github.com/go-xorm/xorm"
)

// MSSQLs ..
type MSSQLs map[string]*MSSQL

// Get MSSQL
func (adapters MSSQLs) Get(name string) (result *MSSQL) {
	if adapter, ok := adapters[name]; ok {
		result = adapter
	} else {
		panic("Không tìm thấy config MSSQL " + name)
	}
	return
}

// MSSQL ..
type MSSQL struct {
	Name    string `mapstructure:"name"`
	Server  string `mapstructure:"server"`
	Port    int    `mapstructure:"port"`
	DBName  string `mapstructure:"dbname"`
	User    string `mapstructure:"user"`
	Pass    string `mapstructure:"password"`
	Session *xorm.Engine
}

var (
	onceSQL      map[string]*sync.Once
	onceSQLMutex = sync.RWMutex{}
)

func init() {
	onceSQL = make(map[string]*sync.Once)
}

func (config MSSQL) isExist() bool {
	return config.Name != ""
}

// Init ..
func (config *MSSQL) Init() {
	if onceSQL[config.Name] == nil {
		onceSQL[config.Name] = &sync.Once{}
	}
	onceSQL[config.Name].Do(func() {
		onceSQLMutex.Lock()
		log.Printf("[%s][%s] MSSQL [connecting]\n", config.Name, config.Server)
		connString := fmt.Sprintf("server=%s;user id=%s;password=%s;port=%d;database=%s", config.Server, config.User, config.Pass, config.Port, config.DBName)
		xormEngine, err := xorm.NewEngine("mssql", connString)
		if err != nil {
			log.Printf("[%s][%s] MSSQL [error]: %s", config.Name, config.Server, err)
			time.Sleep(1 * time.Second)
			onceSQL[config.Name] = &sync.Once{}
			onceSQLMutex.Unlock()
			config.Init()
		} else {
			config.Session = xormEngine
			config.Session.SetMaxOpenConns(100)
			config.Session.SetMaxIdleConns(5)
			//connect thử
			errPing := config.Session.Ping()
			if errPing != nil {
				log.Printf("[%s][%s] MSSQL ping [error]: %s", config.Name, config.Server, errPing)
				time.Sleep(1 * time.Second)
				onceSQL[config.Name] = &sync.Once{}
				onceSQLMutex.Unlock()
				config.Init()
				return
			}
			log.Printf("[%s][%s] MSSQL [connected]\n", config.Name, config.Server)
			onceSQLMutex.Unlock()
		}
	})
}

// GetSQLSession func
func (config MSSQL) GetSQLSession() (session *xorm.Session) {
	if config.Session != nil {
		session = config.Session.NewSession()
	} else {
		panic(fmt.Errorf("[%s][%s] Chưa init MSSQL", config.Name, config.Server))
	}
	return
}
